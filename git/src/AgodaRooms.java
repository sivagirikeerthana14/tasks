import java.time.Duration;
import java.time.LocalDateTime;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AgodaRooms {
	public static void main(String[] args) {
		//	ChromeOptions options = new ChromeOptions();
		//	options.addArguments("--incognito");
		//WebDriver driver = new ChromeDriver(options);
		WebDriver driver =new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
		driver.get("https://www.agoda.com/?ds=mbyCP%2F4saQrWvJGo");
		driver.findElement(By.xpath("//input[@aria-label='Enter a destination or property']")).sendKeys("banglore");
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(100));
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//button[contains(text(),'No thanks')]"))));
		driver.findElement(By.xpath("//button[contains(text(),'No thanks')]")).click();
		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).perform();
		driver.findElement(By.xpath("//button[@aria-label='Previous Month']")).click();
		int day = LocalDateTime.now().getDayOfMonth();
		String mon=LocalDateTime.now().getMonth().toString();
		String month= mon.substring(0, 1)+(mon.substring(1,mon.length())).toLowerCase();
		int  year = LocalDateTime.now().getYear();
		driver.findElement(By.xpath("//div[text()='"+month+" "+ year+"']/..//span[text()='"+day+""+"']")).click();
		day++;
		driver.findElement(By.xpath("//div[text()='"+month+" "+ year+"']/..//span[text()='"+day+""+"']")).click();
		driver.findElement(By.xpath("//p[text()='Room']/../..//button[@aria-label='Add']")).click();
		driver.findElement(By.xpath("//p[text()='Adults']/../..//button[@aria-label='Add']")).click();
		driver.findElement(By.xpath("//p[text()='Adults']/../..//button[@aria-label='Add']")).click();
		driver.findElement(By.xpath("//p[text()='Children']/../..//button[@aria-label='Add']")).click();
		Select select1age = new Select(driver.findElement(By.xpath("//span[text()='Age of Child 1']/../..//select")));
		select1age.selectByValue("7");
		driver.findElement(By.xpath("//p[text()='Child']/../..//button[@aria-label='Add']")).click();
		Select select2age = new Select(driver.findElement(By.xpath("//span[text()='Age of Child 2']/../..//li//select")));
		select2age.selectByValue("8");
		driver.findElement(By.xpath("//span[text()='SEARCH']")).click();
		driver.manage().window().minimize();
		driver.quit();		
	}
}