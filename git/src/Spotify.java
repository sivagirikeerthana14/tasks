import java.io.File;
import java.io.IOException;
import java.time.Duration;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class Spotify {
public static void main(String[] args) throws IOException {
	WebDriver driver = new ChromeDriver();
	driver.manage().window().maximize();
	driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
	driver.get("https://open.spotify.com/");
	driver.findElement(By.xpath("//span[text()='Log in']")).click();
	driver.findElement(By.id("login-username")).sendKeys("sivagirijeeva14@gmail.com");
	driver.findElement(By.id("login-password")).sendKeys("Keerthi@123");
	driver.findElement(By.xpath("//span[@class='Type__TypeElement-sc-goli3j-0 cOJqPq sc-jSUZER sc-gKPRtg hJfyeq hgatVV']")).click();
	driver.findElement(By.xpath("//span[text()='Search']")).click();
	Actions act = new Actions(driver);
	act.sendKeys(driver.switchTo().activeElement(), "happy birthday").sendKeys(Keys.ENTER).perform();
	driver.findElement(By.xpath("//div[@id='onetrust-close-btn-container']")).click();
	WebElement image = driver.findElement(By.xpath("//button[@aria-label='Play Happy Birthday To You - Classic Version by Happy Birthday Songs']"));
	File tempimage = image.getScreenshotAs(OutputType.FILE);
	File permenentimage = new File("./Spotifyimages/onlyimage.png");
	FileUtils.copyFile(tempimage, permenentimage);
	WebElement imgWithLinkText = driver.findElement(By.xpath("//button[@aria-label='Play Happy Birthday To You - Classic Version by Happy Birthday Songs']/../../../.."));
	File tempimgWithLinkText = imgWithLinkText.getScreenshotAs(OutputType.FILE);
	File permenentimgWithLinkText = new File("./Spotifyimages/imgWithLinkText.png");
	FileUtils.copyFile(tempimgWithLinkText, permenentimgWithLinkText);
	TakesScreenshot ts = (TakesScreenshot) driver;
	File tempss = ts.getScreenshotAs(OutputType.FILE);
	File perss = new File("./Spotifyimages/webpage.png");
	FileUtils.copyFile(tempss, perss);	
	driver.manage().window().minimize();
	driver.quit();
}
}
